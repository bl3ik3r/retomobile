package com.androidcer.retomobile.database;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.androidcer.retomobile.model.Cliente;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class DataBase {

    public static DataBase instace;
    DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

public static DataBase getInstance(){

    if(instace ==null){

        instace = new DataBase();

    }
    return instace;

}


    public  void writeNewUser(Cliente cliente,Context context) {

        rootRef.child("Cliente").push().setValue(cliente).addOnSuccessListener(unused -> {

            Toast.makeText(context,"Se registro correctamente",Toast.LENGTH_SHORT).show();


        }).addOnFailureListener(e -> {

            Toast.makeText(context,"Se registro correctamente",Toast.LENGTH_SHORT).show();

        });

    }


}
