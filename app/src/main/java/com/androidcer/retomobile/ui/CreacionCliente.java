package com.androidcer.retomobile.ui;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.androidcer.retomobile.R;
import com.androidcer.retomobile.ViewModel.ViewModelDFormulario;
import com.androidcer.retomobile.model.Cliente;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Objects;

public class CreacionCliente extends AppCompatActivity {


    final Calendar myCalendar = Calendar.getInstance();

    private ViewModelDFormulario viewModelDFormulario;

    Toolbar toolBar;
    Button btnRegistrar;
    TextInputEditText edit_nombre;
    TextInputEditText edit_apellido;
    TextInputEditText edit_edad;
    TextInputEditText edit_fecha;
    ImageButton imgCalendar;

    FirebaseDatabase database;
    DatabaseReference myRef;

    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creacion_cliente);

        init();
        listeners();







    }

    public void init() {

        viewModelDFormulario = new ViewModelProvider(this).get(ViewModelDFormulario.class);

        toolBar = findViewById(R.id.my_toolbar);
        btnRegistrar = findViewById(R.id.btn_complete);
        edit_nombre = findViewById(R.id.txt_name_sub);
        edit_apellido = findViewById(R.id.txt_lastName_sub);
        edit_edad = findViewById(R.id.txt_age_sub);
        edit_fecha = findViewById(R.id.txt_date_sub);
        imgCalendar = findViewById(R.id.imageButton);

        toolBar.setTitle("Creación de Cliente");

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(myCalendar.getTime());

            edit_fecha.setText(selectedDate);


        };


    }

    public void listeners(){
        imgCalendar.setOnClickListener(view -> {

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();

        });


        btnRegistrar.setOnClickListener(view -> {
            Cliente cliente = new Cliente();
            cliente.setNombre(Objects.requireNonNull(edit_nombre.getText()).toString());
            cliente.setApellido(Objects.requireNonNull(edit_apellido.getText()).toString());
            cliente.setEdad(Objects.requireNonNull(edit_edad.getText()).toString());
            cliente.setFechaNacimimento(Objects.requireNonNull(edit_fecha.getText()).toString());
            viewModelDFormulario.setFormulario(cliente,view.getContext());


        });


    }


}