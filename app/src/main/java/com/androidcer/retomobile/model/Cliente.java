package com.androidcer.retomobile.model;

import java.util.Date;

public class Cliente {

    String Nombre;
    String Apellido;
    String Edad;
    String fechaNacimimento;

    public Cliente() {

    }

    public Cliente(String nombre, String apellido, String edad, String fechaNacimimento) {
        Nombre = nombre;
        Apellido = apellido;
        Edad = edad;
        this.fechaNacimimento = fechaNacimimento;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        Edad = edad;
    }

    public String getFechaNacimimento() {
        return fechaNacimimento;
    }

    public void setFechaNacimimento(String fechaNacimimento) {
        this.fechaNacimimento = fechaNacimimento;
    }
}
