package com.androidcer.retomobile.ViewModel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.androidcer.retomobile.database.DataBase;
import com.androidcer.retomobile.model.Cliente;

public class ViewModelDFormulario extends ViewModel {

    private final MutableLiveData<Cliente> user;
    private final DataBase mRepo;

    public ViewModelDFormulario() {

        user = new MutableLiveData<>();
        mRepo = DataBase.getInstance();


    }

    public LiveData<Cliente> getCliente() {
        return user;

    }

    public void setFormulario(Cliente cliente, Context context) {
        mRepo.writeNewUser(cliente,context);
    }


}
